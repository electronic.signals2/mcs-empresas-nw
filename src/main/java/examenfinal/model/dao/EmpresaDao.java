package examenfinal.model.dao;

import java.util.List;

import examenfinal.model.Empresa;


public interface EmpresaDao {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);
	void deleteEmpresa(Integer id);

}
