package examenfinal.model.dao;

import java.util.List;

import examenfinal.model.Recibo;


public interface ReciboDao {
	
	List<Recibo> getAllRecibos(Integer id);
	Recibo getRecibo(Integer id);
	void saveRecibo(Recibo empresa);
	void deleteRecibo(Integer id);

}
