package examenfinal.model.dao.rowmapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import examenfinal.model.Recibo;
 
public class ReciboRowMapper implements RowMapper<Recibo>{

	@Override
	public Recibo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Recibo persona = new Recibo();
		
		persona.setIdEmpresa(rs.getInt("id_empresa"));
		persona.setFgRetencion(rs.getString("fg_retencion"));
		persona.setMontoEmitido(rs.getBigDecimal("monto_emitido"));
		persona.setRnoRecibo(rs.getString("rno_recibo"));
		
		return persona;
	}

}
