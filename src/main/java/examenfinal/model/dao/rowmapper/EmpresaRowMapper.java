package examenfinal.model.dao.rowmapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import examenfinal.model.Empresa;


public class EmpresaRowMapper implements RowMapper<Empresa>{

	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		Empresa persona = new Empresa();
		
		persona.setIdEmpresa(rs.getInt("id_empresa"));
		persona.setRuc(rs.getString("ruc"));
		persona.setRazonSocial(rs.getString("razon_social"));
		persona.setEstadoActual(rs.getString("estado_actual"));
		
		return persona;
	}

}
