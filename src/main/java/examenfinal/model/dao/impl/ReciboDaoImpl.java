package examenfinal.model.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import examenfinal.model.Empresa;
import examenfinal.model.Recibo;
import examenfinal.model.dao.ReciboDao;
import examenfinal.model.dao.rowmapper.EmpresaRowMapper;
import examenfinal.model.dao.rowmapper.ReciboRowMapper;


@Repository
public class ReciboDaoImpl extends JdbcDaoSupport implements ReciboDao {
	public ReciboDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	@Override
	public List<Recibo> getAllRecibos(Integer id) {
		// TODO Auto-generated method stub
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Recibo> listaRecibos = new ArrayList<Recibo>();
		
		String sql = " SELECT id_empresa, rno_recibo, monto_emitido, fg_retencion\n" + 
				" FROM microservicios.recibo where id_empresa='"+id+"'";
		
		try {
			
			RowMapper<Recibo> reciboRow = new ReciboRowMapper();
			listaRecibos = getJdbcTemplate().query(sql, reciboRow);
			logger.debug("Se han listado "+listaRecibos.size()+" recibos");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaRecibos;
	}

	@Override
	public Recibo getRecibo(Integer id) {
		// TODO Auto-generated method stub
		logger.debug("::::: Mensaje de prueba :::::::");
		Recibo empresa = new Recibo();	
		List<Recibo> listaEmpresas = new ArrayList<Recibo>();
		
		String sql = " SELECT id_empresa, rno_recibo, monto_emitido, fg_retencion\n" + 
				" FROM microservicios.recibo where id_empresa='"+id+"'";
				
		try {
			
			RowMapper<Recibo> empresaRow = new ReciboRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			
			empresa = listaEmpresas.get(0);
			
			logger.debug("Se ha traido a la recibo "+listaEmpresas.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return empresa;
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		// TODO Auto-generated method stub
		String sql = "insert into microservicios.recibo (id_empresa, rno_recibo, monto_emitido, fg_retencion) "  
				+ "values (?, ?, ?,?);";
		
		Object[] params = { recibo.getIdEmpresa(), recibo.getRnoRecibo(), recibo.getMontoEmitido(), recibo.getFgRetencion()};
		int[] tipos = {Types.INTEGER , Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la recibo "+recibo.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}

	@Override
	public void deleteRecibo(Integer id) {
		// TODO Auto-generated method stub

	}

}
