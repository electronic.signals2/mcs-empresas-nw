package examenfinal.model;

import java.math.BigDecimal;

public class Recibo {
	private int idEmpresa;
	private String rnoRecibo;
	private BigDecimal montoEmitido;
	private String fgRetencion;
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public String getRnoRecibo() {
		return rnoRecibo;
	}
	public void setRnoRecibo(String rnoRecibo) {
		this.rnoRecibo = rnoRecibo;
	}
	public BigDecimal getMontoEmitido() {
		return montoEmitido;
	}
	public void setMontoEmitido(BigDecimal montoEmitido) {
		this.montoEmitido = montoEmitido;
	}
	public String getFgRetencion() {
		return fgRetencion;
	}
	public void setFgRetencion(String fgRetencion) {
		this.fgRetencion = fgRetencion;
	}
	
}
