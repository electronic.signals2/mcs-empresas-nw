package examenfinal.service;

import java.util.List;

import examenfinal.model.Recibo;



public interface ReciboService {
	
	List<Recibo> getAllRecibos(Integer id);
	Recibo getRecibo(Integer id);
	void saveRecibo(Recibo persona);
	void deleteRecibo(Integer id);
	
}
