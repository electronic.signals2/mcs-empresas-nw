package examenfinal.service;

import java.util.List;

import examenfinal.model.Empresa;

 
public interface EmpresaService {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa persona);
	void deleteEmpresa(Integer id);
	
}
