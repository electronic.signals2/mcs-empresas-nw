package examenfinal.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import examenfinal.model.Recibo;

import examenfinal.model.dao.impl.ReciboDaoImpl;
import examenfinal.service.ReciboService;


@Service
public class ReciboServiceImpl implements ReciboService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ReciboDaoImpl _reciboDao;
	
	@Override
	public List<Recibo> getAllRecibos(Integer id) {
		
		return _reciboDao.getAllRecibos(id);
	}

	@Override
	public Recibo getRecibo(Integer id) {
		
		return _reciboDao.getRecibo(id);
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		try {
			_reciboDao.saveRecibo(recibo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}

	@Override
	public void deleteRecibo(Integer id) {
		try {
			_reciboDao.deleteRecibo(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}

}
