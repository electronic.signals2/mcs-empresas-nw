package examenfinal.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import examenfinal.model.Empresa;


//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/examen/empresabeta")
//@Api(description = "Api para mantenimiento de empresa - Ali Vargas")
public class EmpresaBetaController {
	
	
	@GetMapping(value = "/all", produces = "application/json")
	//@ApiOperation("Lista general de empresas")
	public List<Empresa> getAllEmpresas(){
		List<Empresa> lista = new ArrayList<>();
		Empresa emp = new Empresa();
		emp.setEstadoActual("1");
		emp.setRazonSocial("EMPRESA");
		emp.setIdEmpresa(1);
		emp.setRuc("10418791093");
		lista.add(emp);
		return lista;
	}

}
