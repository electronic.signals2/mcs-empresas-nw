package examenfinal.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import examenfinal.model.Empresa;
import examenfinal.model.Recibo;
import examenfinal.service.impl.EmpresaServiceImpl;
import examenfinal.service.impl.ReciboServiceImpl;



//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/examen/empresa")
//@Api(description = "Api para mantenimiento de empresa - Ali Vargas")
public class EmpresaController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@Autowired
	private ReciboServiceImpl _reciboService;
	
	@GetMapping(value = "/all", produces = "application/json")
	//@ApiOperation("Lista general de empresas")
	public Map<String,Object> getAllEmpresas(){
		Map<String,Object> map = new HashMap<>();
		map.put("codigo_servicio", "0000");
		List<Empresa>  lst = _empresaService.getAllEmpresas();
		map.put("empresas", lst);
		return map;
	}
	
	@GetMapping(value = "/get/{id}", produces = "application/json")	
	//@ApiOperation("Buscar a una empresa por ID")
	public Empresa getEmpresa( @PathVariable ("id") Integer id){
		Map<String,Object> map = new HashMap<>();
		map.put("codigo_servicio", "0000");
		Empresa emp = _empresaService.getEmpresa(id);
		List<Recibo> lstrecibo = _reciboService.getAllRecibos(id);
		emp.setListaRecibos(lstrecibo);
		return emp;
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	//@ApiOperation("Guardar nueva empresa")
	public Map<String,String> saveEmpresa(@RequestBody Empresa empresa){
		
		Map<String,String> map = new HashMap<>();
		_empresaService.saveEmpresa(empresa);
		map.put("codigo_servicio", "0000");
		map.put("descripcion","Empresa registrada con �xito");
		_empresaService.getAllEmpresas();
		return map;
	}	
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")	
	public List<Empresa> deleteEmpresa(@PathVariable ("id") Integer id){
		
		_empresaService.deleteEmpresa(id);
		
		return _empresaService.getAllEmpresas();
	}	

}
