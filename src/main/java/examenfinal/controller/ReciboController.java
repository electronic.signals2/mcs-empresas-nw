package examenfinal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import examenfinal.model.Recibo;
import examenfinal.service.impl.ReciboServiceImpl;


/*import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
*/
@RestController
@RequestMapping("/examen/recibo")
//@Api(description = "Api para mantenimiento de recibo - Ali Vargas")
public class ReciboController {
	
	@Autowired
	private ReciboServiceImpl _reciboService;
	
	@GetMapping(value = "/all", produces = "application/json")
	//@ApiOperation("Lista general de recibos")
	public List<Recibo> getAllRecibos(){
		return null;
	}
	
	@GetMapping(value = "/get/{id}", produces = "application/json")	
	//@ApiOperation("Buscar a una recibo por ID")
	public Recibo getRecibo(  @PathVariable ("id") Integer id){
		return _reciboService.getRecibo(id);
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	//@ApiOperation("Guardar nueva recibo")
	public void saveRecibo(@RequestBody Recibo recibo){
		
		_reciboService.saveRecibo(recibo);
		
		return ;
	}	
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")	
	public List<Recibo> deleteRecibo(@PathVariable ("id") Integer id){
		
		_reciboService.deleteRecibo(id);
		
		return null;
	}	

}
